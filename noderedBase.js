const logger = new (require("node-red-contrib-logger"))("noderedBase");
logger.sendInfo("Copyright 2022 Jaroslav Peter Prib");

function noderedBase(RED, node) {
	this.node = node;
	this.RED = RED;
	this.nodeContext = this.node.context();
	this.flow = this.nodeContext.flow;
	this.global = this.nodeContext.global;
	return this;
}
noderedBase.prototype.evalFunction = function (id, mapping = null) {
	if (logger.active) logger.send({ label: "evalFunction", id: id, mapping: mapping });
	try {
		if (mapping == null) throw Error("mapping function is null");
		const node = this.node;
		const nodeContext = this.context;
		const flow = this.flow;
		const global = this.global;
		const RED = this.RED;
		return eval(mapping).bind(this);
	} catch (ex) {
		logger.sendError({ label: "evalFunction error", id: id, mapping: mapping, error: ex.message })
		throw Error(id + " " + ex.message);
	}
}
noderedBase.prototype.error = function (ex, shortMessage = ex.message) {
	if (logger.active) logger.send({ label: "error", node: { id: this.node.id, name: this.node.name }, shortMessage: shortMessage, message: ex.message, stack: ex.stack });
	this.node.error(ex.message);
	this.node.status({ fill: "red", shape: "ring", text: shortMessage });
}
noderedBase.prototype.callFunctionInText = function (propertyName) {
	if (!this.node.hasOwnProperty(propertyName)) throw Error("no value for " + propertyName);
	const property = this.node[propertyName];
	const propertyTypeName = propertyName + "Type";
	if (!this.node.hasOwnProperty(propertyTypeName)) throw Error("no value for " + propertyTypeName);
	const propertyType = this.node[propertyTypeName];
	switch (propertyType) {
		case "num":
		case "str":
		case "date":
		case "json":
		case "jsonata":
		case "bin":
		case "bool":
		case "re":
			return "(msg)=>RED.util.evaluateNodeProperty(node." + propertyName + ",\"" + propertyType + "\",node,msg)"
		//		return "(msg)=>RED.util.evaluateNodeProperty(node."+propertyName+",node."+propertyTypeName+",node,msg,function(err,res){
		//      (err,res)=>err?this.error(err,msg)?done(res)
		case "genid":
			return "()=>RED.util.generateId()";
		/*
			case "jsonata":
					const jsonata=require("jsonata");
					const jsonataExpression=jsonata("+property+");
					return this.evalFunction(propertyName,"(msg)=>jsonataExpression({msg:msg,node:thisObject.nodeContext,flow:thisObject.flow,global:thisObject.global}})");
		*/
		case "node":
			return "()=>nodeContext.get(" + property + ")";
		case "flow":
			if (this.flow == null) throw Error("context store may be memoryonly so flow doesn't work")
			return "()=>flow.get(" + property + ")";
		case "global":
			return "()=>global.get(" + property + ")";
		case "env":
			return "()=>env.get(" + property + ")";
		case "msg":
			return "(msg)=>msg." + property;
		/*
			case "re":
				return "(msg)=>\\"+property+"\\";
		*/
		case "genData":
			if (!this.dummyjson) this.dummyjson = require("dummy-json");
			const testText = this.dummyjson.parse(property);
			return "(msg)=>this.dummyjson.parse(node." + propertyName + ")";
		case "genDataJSON":
			if (!this.dummyjson) this.dummyjson = require("dummy-json");
			const testJSON = JSON.parse(this.dummyjson.parse(property));
			return "(msg)=>JSON.parse(this.dummyjson.parse(node." + propertyName + "))";
		default:
			logger.sendInfo({ label: "setData unknown type", name: propertyName, type: propertyType, value: property });
			throw Error("unknown type " + propertyType + " for " + propertyTypeName)
	}
}
function argsArray(node, msg) {
	const args = [];
	node.argFunction.forEach(callFunction => {
		const result = callFunction(msg);
		args.push(result);
	});
	return args;
}
noderedBase.prototype.setSource = function (propertyName) {
	const node = this.node;
	if (!node.hasOwnProperty(propertyName)) {
		logger.sendWarn({ label: "setSource", error: "property on node not found", id: node.id, propertyName: propertyName })
		return this;
	}
	const getPropertyName = "get" + propertyName.substr(0, 1).toUpperCase() + propertyName.substr(1);
	node[getPropertyName] = this.evalFunction(propertyName, this.callFunctionInText(propertyName));
	if (logger.active) logger.send({
		label: "setSource", node: { id: node.id, name: node.name },
		functionName: getPropertyName, function: node[getPropertyName].toString()
	});
	return this;
}
noderedBase.prototype.setTarget = function (propertyName) {
	const node = this.node;
	const setPropertyName = "set" + propertyName.substr(0, 1).toUpperCase() + propertyName.substr(1);
	if (!node.hasOwnProperty(propertyName)) {
		logger.sendWarn({
			label: "setTarget", error: "property on node not found",
			id: node.id, propertyName: propertyName, functionName: setPropertyName
		})
		return this;
	};
	const propertyType = node[propertyName + "Type"];
	const property = node[propertyName];
	let callFunction;
	switch (propertyType) {
		case "node":
			callFunction = "data=>nodeContext.set(" + property + ",data)";
			break;
		case "flow":
			if (flow) throw Error("context store may be memoryonly so flow doesn't work")
			callFunction = "data=>flow.set(" + property + ",data)";
			break;
		case "global":
			callFunction = "data=>global.set(" + property + ",data)";
			break;
		case "msg":
			callFunction = "(data,msg)=>{msg." + property + "=data;}";
			break;
		default:
			logger.sendWarn({ label: "setData unknown type", name: propertyName, type: propertyType, property: property });
			return;
	}
	node[setPropertyName] = this.evalFunction(propertyName, callFunction);

	if (logger.active) logger.send({ label: "setTarget", node: { id: node.id, name: node.name }, functionName: setPropertyName, function: node[setPropertyName].toString() });
	return this;
}
noderedBase.prototype.setArgs = function (propertyName) {
	this.args[propertyName] = [];
	this.args[propertyName].forEach(property => {
		node.argFunction.push(this.evalFunction(propertyName, this.callFunctionInText(propertyName)));
	})
	return this;
}
module.exports = noderedBase;