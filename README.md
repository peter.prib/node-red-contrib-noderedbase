
# [node-red-contrib-prib-noderedBase][2]

Base module of functions when developing own nodes in [Node-Red][1]


## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

------------------------------------------------------------


# Usage
 
	
		const noderedBase=require("noderedBase");
	    RED.nodes.createNode(this,n);
	    const node=Object.assign(this,n);
		node._base=new noderedBase(RED,node);
		try{
			this._base.setSource("key").setTarget("target")
		} catch(ex) {
			node._base.error(ex,"Invalid setup "+ex.message);
			return;
		}
		node.on("input",function (msg) {
			try{
				const key=node.getKey(msg);
				node.setTarget(key,msg);
				node.send(msg);
			} catch(ex) {
				msg.error=ex.message;
				node.send([null,msg]);
		});


------------------------------------------------------------

# Installation

Run the following command in the root directory of your Node-RED install

    npm install node-red-contrib-noderedbase

# Tests

Test/example flow in test/generalTest.json

------------------------------------------------------------

# Version

0.0.1 initial base  

------------------------------------------------------------

# Author

[Peter Prib][3]

[1]: http://nodered.org "node-red home page"

[2]: https://www.npmjs.com/package/node-red-contrib-nodered "source code"

[3]: https://gitlab.com/peter.prib "base gitlab"
